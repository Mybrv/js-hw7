const palindrom = prompt("Enter palindrome")

function isPalindrome (str) {
 const arrOfSymbols = str.toLowerCase().split("").filter(item => item !== " ").join("")
    console.log(arrOfSymbols)
    const reverseArrOfSymbols = [...arrOfSymbols].reverse().join("")
    console.log(reverseArrOfSymbols);
    const result = arrOfSymbols === reverseArrOfSymbols;
   if (result) {
      console.log("This is palindrome")
   } else {
      console.log("This is not palindrome");
   }
}

isPalindrome(palindrom)

//2. 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну
// довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане
// в нагоді для валідації форми. Приклади використання функції:
// // Рядок коротше 20 символів
//
// funcName('checked string', 20); // true
//
// // Довжина рядка дорівнює 18 символів
//
// funcName('checked string', 10); // false

function strLenghts (str, maxlenght) {
    if (str.length <= maxlenght) {
        console.log(`Довжина рядка дорівнює ${str.length}` + " " + true)
    } else {
        console.log(`Довжина рядка дорівнює ${str.length}` + " " + false)
    }
}

strLenghts("аргентина манит негра", 22)

//3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через
// prompt. Функція повина повертати значення повних років на дату виклику функцію.

function whatIsYouAge() {
    const today = new Date();
    const DateInput = prompt("Введите вашу дату рождения (в формате ГГГГ-ММ-ДД):");
    const birthDate = new Date(DateInput);
    if (isNaN(birthDate.getTime())) {
        alert("Неверный формат даты. Попробуйте еще раз.");
        return;
    }
    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDifference = today.getMonth() - birthDate.getMonth();
    const dayDifference = today.getDate() - birthDate.getDate();
    if (monthDifference < 0 || (monthDifference === 0 && dayDifference < 0)) {
        age--;
    }
    alert(`Вам ${age} лет.`);
}

whatIsYouAge();